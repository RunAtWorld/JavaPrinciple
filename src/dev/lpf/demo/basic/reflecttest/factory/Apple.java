package dev.lpf.demo.basic.reflecttest.factory;

/**
 * Apple
 *
 *
 */
public class Apple implements Fruit {
    @Override
    public void eat() {
        System.out.println("eat Apple");
    }
}
