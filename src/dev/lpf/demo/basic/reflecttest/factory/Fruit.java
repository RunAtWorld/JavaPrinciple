package dev.lpf.demo.basic.reflecttest.factory;

/**
 * Fruit
 *
 *
 */
public interface Fruit {
    public void eat();
}
