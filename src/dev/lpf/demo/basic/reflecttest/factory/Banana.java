package dev.lpf.demo.basic.reflecttest.factory;

/**
 * Banana
 *
 *
 */
public class Banana implements Fruit {
    @Override
    public void eat() {
        System.out.println("eat Banana");
    }
}
