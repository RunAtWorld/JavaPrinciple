package dev.lpf.demo.basic.reflecttest.proxy;

/**
 * 项目接口
 */
public interface ISubject {
    public String say(String subjectName);
}
