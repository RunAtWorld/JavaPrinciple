package dev.lpf.designpattern.proxy.staticproxy;

public interface Subject {
	public void request();
}
