package dev.lpf.designpattern.proxy.dynamicproxy;

public interface IAdvice {
	public void exec();
}