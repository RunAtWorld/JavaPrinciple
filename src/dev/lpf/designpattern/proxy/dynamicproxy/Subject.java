package dev.lpf.designpattern.proxy.dynamicproxy;

public interface Subject {
	public void doSomething(String str);
}
