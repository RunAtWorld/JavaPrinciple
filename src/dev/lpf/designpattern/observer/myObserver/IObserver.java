package dev.lpf.designpattern.observer.myObserver;

public interface IObserver {
	public void update(String str);
}
