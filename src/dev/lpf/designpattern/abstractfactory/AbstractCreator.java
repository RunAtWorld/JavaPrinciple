package dev.lpf.designpattern.abstractfactory;

public abstract class AbstractCreator{
	public abstract IProduct createProductA();
	public abstract IProduct createProductB();
}
