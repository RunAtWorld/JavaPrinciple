package dev.lpf.designpattern.singleton;

/**
 * enum mode
 * @author lipengfei
 *
 */
public enum EasySingleton {
	INSTANCE,
	INSTANCE2;
}
