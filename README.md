# Java练习
## 0.[Java基本使用练习](src/dev/lpf/demo/README.md)

|Basic     |Others |
|----------|----------|
|1. JavaSE |  Maven   |
|2. JSP	   ||
|3. Spring ||
|4. Strus  ||
|5. MyBatis||
|6. SpringBoot||
|7. SpringCloud||

## 1.数据结构与算法
1. 链表
1. 树
1. 图
1. 排序
1. 动态规划
1. 贪心

## 2.[笔试练习题](src/dev/lpf/exams/README.md)

## 3.[Leetcode](src/dev/lpf/leetcode/README.md)

## 4.[OJ](src/dev/lpf/oj/README.md)

## 5.[设计模式](src/dev/lpf/designpattern/README.md)
1. [单例模式](src/dev/lpf/designpattern/singleton)
1. [工厂方法模式](src/dev/lpf/designpattern/factorymethod)
1. [抽象工厂模式](src/dev/lpf/designpattern/abstractfactory)
1. [模板方法模式](src/dev/lpf/designpattern/model)
1. [代理模式](src/dev/lpf/designpattern)：[静态代理](dev/lpf/designpattern/proxy/staticproxy)和[动态代理](dev/lpf/designpattern/proxy/dynamicproxy)
1. [适配器模式](src/dev/lpf/designpattern/adapter):[类适配器模式](dev/lpf/designpattern/adapter/classAdapter)和[对象适配器模式](dev/lpf/designpattern/adapter/objectAdapter)
2. [建造者模式](src/dev/lpf/designpattern/builder)
2. [观察者模式](src/dev/lpf/designpattern/observer)
2. [原型模式](src/dev/lpf/designpattern/prototype)

## 6.[常用工具类](src/dev/lpf/utils/README.md)